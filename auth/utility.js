const home = require('../reports-db.js')
const jwt = require('jsonwebtoken')

exports.getReport = async (req, res, next) => {

  const { search_term } = req.body

  try {
      report = home.getReport(search_term)

      report.then(data => {

         if (data.length == 0) {
             res.status(400).json({
             message: "Report fetch failed. No report found.",

             })
         }

         else {

           // Here we need to parse the SQL result for the key report info. This way, it can be processed by the GUI and displayed to the user.
           // let reportId = data[0].report_id
           res.status(201).json({
             message: "Report fetched successfully."
           });
         }

    })

  } catch (err) {
    res.status(401).json({
      message: "Report fetch failed.",
      error: err.message
    })
  }
 }

exports.getAllReports = async (req, res, next) => {

    try {
        report = home.getAllReports()

	console.log(req.cookies)
        report.then(data => {

            if(data.length == 0) {
                console.log('Nothing here chap.')
            }

            else {

                // Here we need to parse the info for the needed results.

		res.status(201).json({
                    message: "All reports fetched successfully.",
                    data: JSON.stringify(data)
                });
            }
        })
    }

    catch (err) {
    res.status(401).json({
      message: "Report fetch failed.",
      error: err.message
    })
    }
}

exports.createReport = async (req, res, next) => {

    const { report_id } = req.body

    try {
        report = home.createReport(report_id)

        res.status(200).json({
          message: "Report successfully created.."
        })
    }

    catch (err) {
    res.status(401).json({
      message: "Report creation failed.",
      error: err.message
    })
    }
}
