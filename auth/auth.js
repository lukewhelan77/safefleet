const home = require('../user-db.js')
const app = require('../server.js')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const jwtSecret = '7c3fac40e1d301e4bbba3cce303313115c5226f0435cd89f5157815607f1aaadb0726c'

exports.register = async (req, res, next) => {

  const { username, password, role } = req.body

  if (password.length < 6) {
    return res.status(400).json({ message: "Password less than 6 characters" })
  }

  try {
    bcrypt.hash(password, 10).then(async (hash) => {
      home.createUser(username, hash, role)

      const maxAge = 3 * 60 * 60;

      const token = jwt.sign(
        { userName: username, userRole: role },
        jwtSecret,
        {
          expiresIn: maxAge,
        }
      );

      res.cookie("jwt", token, {
        httpOnly: true,
        maxAge: maxAge * 1000, // 3hrs in ms
      });

      res.status(201).json({
        message: "User successfully created"
      });

    })

  } catch (err) {
    res.status(401).json({
      message: "User not successful created",
      error: err.message
    })
  }
}

exports.login = async (req, res, next) => {
  const { username, password } = req.body

  // Check if username and password is provided.

  if (!username || !password) {
    return res.status(400).json({
      message: "Username or Password not present",
    })
  }

  try {
    x = home.getUser(username, password) // Get the user

    x.then(data => {
        if (data.length == 0) {

            res.status(400).json({
            message: "Login not succesful",
            error: "Username or password incorrect",
          })
        }

        else {

          let userPassword = data[0].userPassword

          // We compare the given password with the hashed password.
          bcrypt.compare(password, userPassword).then(function (result) {
              if (result) {

                let username = data[0].userName
                let userrole = data[0].userPassword

                const maxAge = 3 * 60 * 60;
                const token = jwt.sign(
                  { userName: username, userRole: userrole },
                  jwtSecret,
                  {
                    expiresIn: maxAge,
                  }
                );

                res.cookie("jwt", token, {
                  httpOnly: true,
                  sameSite: 'none',
                  secure: true,
                  maxAge: maxAge * 1000,
                });

	        res.render("homepage")
              }

              else {
                res.status(400).json({
                  message: "User not succesfully logged in."
                });

              }
          })
        }
       })

  }

  catch(err) {
    res.status(401).json({
      message: "User not succesfully logged in.",
      error: err.message
    })
  }
}
