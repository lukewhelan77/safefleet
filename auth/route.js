const express = require("express")
const router = express.Router()
const { register, login, example } = require("./auth")
const { getReport, getAllReports, createReport } = require("./utility")

router.route("/login").post(login)
router.route("/register").post(register)

/////////

router.route("/createReport").post(createReport)
router.route("/getReport").get(getReport)
router.route("/getAllReports").get(getAllReports)

module.exports = router
