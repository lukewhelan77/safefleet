const cors = require('cors')
const express = require('express')
const app = express()
const fs = require('fs')
require('dotenv').config()
const https = require('https')
const path = require('path')
const jwt = require('jsonwebtoken')
const file = fs.readFileSync('../9EB2AAC7CA255A5C5758936616F0D842.txt')
const cookieParser = require('cookie-parser')
var mysql = require('mysql2');

// Some use calls.
app.use(cors())
app.use(express.json())
app.use(cookieParser())
app.use("/auth", require("./auth/route"))
app.use( express.static( "public" ) );
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.get('/', (req, res) => {
  res.render('login-page')
})

// HTTPs credential setup.

const key = fs.readFileSync('https/private.key')
const cert = fs.readFileSync('https/certificate.crt')

const cred = {
   key,
   cert
}

app.listen(3302)

const httpsServer = https.createServer(cred, app)
httpsServer.listen(8443)
